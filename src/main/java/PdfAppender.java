import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by alacambra on 05.11.17.
 */
public class PdfAppender {

    public static void main(String[] args) throws IOException, COSVisitorException {
        PDDocument document = new PDDocument();
        for (int i = 76; i < 85; i++) {
            System.out.println(String.format("Adding /Users/albertlacambra1/Downloads/Gmail/IMG_60%d.JPG", i));
            addToNewPage(String.format("/Users/albertlacambra1/Downloads/Gmail/IMG_60%d.JPG", i), document);
        }
        document.save("file.pdf");
        document.close();

    }

    private static void addToNewPage(String filePath, PDDocument document) throws IOException {
        InputStream in = new FileInputStream(filePath);
        BufferedImage bimg = ImageIO.read(in);
        float width = bimg.getWidth();
        float height = bimg.getHeight();
        PDPage page = new PDPage(new PDRectangle(width, height));
        document.addPage(page);
        PDXObjectImage img = new PDJpeg(document, new FileInputStream(filePath));
        page.setRotation(90);
        PDPageContentStream contentStream = new PDPageContentStream(document, page);
        contentStream.drawImage(img, 0, 0);
        contentStream.close();
        in.close();
    }
}
